package com.example.ygordacosta.teste;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    public static final String CONTATO = "contato";
    public static final int REQUEST_NOVO =1;
    private ListView listaV;
    private List<Contato> lista = new ArrayList<>();
    private AdapterContato adapter;

    private ContatoDAO dao;
    private Contato contatoselecionado;
    private static Bitmap imagem;
    public  static Bitmap getImagem(){
        return imagem;

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem menuligar = menu.add("Ligar");
    menuligar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
           Intent intent = new Intent(Intent.ACTION_CALL);
           Uri discar = Uri.parse("Tel:"+ contatoselecionado.getTelefone());
           intent.setData(discar);
           startActivity(intent);

            return false;
        }
    });

    MenuItem editar = menu.add("Editar");
    editar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            Intent intent  = new Intent(MainActivity.this,NovoActivity.class);
            intent.putExtra(CONTATO,contatoselecionado);
            startActivity(intent);
            return false;
        }
    });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;}




    public void novo(MenuItem item) {
        Intent intent = new Intent(this, NovoActivity.class);
        startActivityForResult(intent, REQUEST_NOVO);
    }
    @Override
    protected void onResume() {
        super.onResume();
        listaV = findViewById(R.id.Contatos);
        adapter = new AdapterContato(this, lista);
        atualizacao();
        listaV.setAdapter(adapter);


        listaV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posicao, long l) {
                contatoselecionado = (Contato) adapterView.getItemAtPosition(posicao);

                Intent itent = new Intent(MainActivity.this, DetalheActivity.class);
                itent.putExtra(CONTATO,contatoselecionado);
                startActivity(itent);

            }
        });

        listaV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int posicao, long l) {
                contatoselecionado = (Contato) adapterView.getItemAtPosition(posicao);


                return false;


            }
        });

        registerForContextMenu(listaV);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_NOVO){
            switch(resultCode){
                case RESULT_OK:
                   /*
                    Contato contato = (Contato) data.getSerializableExtra(MainActivity.CONTATO);
                    contato.setImagem(NovoActivity.getimagem());
                    */
                    atualizacao();
                 adapter.notifyDataSetChanged();
                 break;

                case RESULT_CANCELED:
                    Toast.makeText(this,"Cancelado.", Toast.LENGTH_LONG).show();
            }

        }}
    private void atualizacao(){

        dao = new ContatoDAO(this);
        lista = dao.getLista();
        dao.close();
    }
    }











