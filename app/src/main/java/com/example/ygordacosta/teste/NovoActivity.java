package com.example.ygordacosta.teste;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;


public class NovoActivity extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1;

     private String camninho;

    private EditText nomecadastro;
    private ImageView imageview;
    private EditText Telefonecadastro;
    private EditText Celularcadastro;
    private EditText Enderecocadastro;
    private Button btnsalvar;
    private ContatoDAO dao;
    private Contato contato;
    private String imagemcadastro;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo2);
        nomecadastro = findViewById(R.id.Nome);
        Telefonecadastro = findViewById(R.id.Telefone);
        Celularcadastro = findViewById(R.id.Celular);
        Enderecocadastro = findViewById(R.id.Endereço);
        btnsalvar = findViewById(R.id.Salvar);
       imageview = findViewById(R.id.Imagem);
       /*
        Intent intent = getIntent();

        contato = (Contato) intent.getSerializableExtra(MainActivity.CONTATO);

        if(contato == null){
            contato = new Contato();
        }
        nomecadastro.setText(contato.getNome());



        if(contato.getImagem() != null){
            Bitmap origem = BitmapFactory.decodeFile(contato.getImagem());
            Bitmap padrao = Bitmap.createScaledBitmap(origem,200,200,true);
            imageview.setImageBitmap(padrao);

        } */
    }


    public void capturing(View view) {

        // deifinir onde o arquivo vai ser gravado .....

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


            String caminhoexterno = Environment.getExternalStorageDirectory().toString();

            imagemcadastro = caminhoexterno + "/" + System.currentTimeMillis() + ".png;";

            File arquivo = new File(imagemcadastro);

            Uri localdearmazenamento = Uri.fromFile(arquivo);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,localdearmazenamento);

            // passa como paramentro onde eu vou salvar
            // /sd/mdeid/ minhafoto.png

            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

          }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ( REQUEST_IMAGE_CAPTURE == requestCode){

            if(resultCode == RESULT_OK){

                Bitmap fotoOrg = BitmapFactory.decodeFile(imagemcadastro);

                Bitmap fotoRdz = Bitmap.createScaledBitmap(fotoOrg , 200,200,true );

                imageview.setImageBitmap(fotoRdz);

            }
        }
    }

    public void salvar(View view) {
        String nome = nomecadastro.getText().toString();
        String telefone = Telefonecadastro.getText().toString();
        String Celular = Celularcadastro.getText().toString();
        String Endereco = Enderecocadastro.getText().toString();
        String imagem = imagemcadastro;

        contato = new Contato();
        contato.setEndereço(Endereco);
        contato.setCelular(Celular);
        contato.setTelefone(telefone);
        contato.setNome(nome);
        contato.setImagem(imagem);

        dao = new ContatoDAO(this);
        dao.salvar(contato);
        dao.close();


        setResult(RESULT_OK);
        finish();
        Toast.makeText(this, "Salvo!", Toast.LENGTH_LONG).show();

    }


    public void cancelar(View view) {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
    }
}
