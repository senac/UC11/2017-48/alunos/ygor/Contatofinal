package com.example.ygordacosta.teste;

import android.graphics.Bitmap;

import java.io.Serializable;



public class Contato implements Serializable {

   private String nome;
     private String telefone;
    private String celular;
     private String Foto;
     private int id;
     private String imagem;

    public Contato() {

    }

    public Contato(String nome, String telefone, String celular, String foto, int id, String imagem, String endereço) {
        this.nome = nome;
        this.telefone = telefone;
        this.celular = celular;
        Foto = foto;
        this.id = id;
        this.imagem = imagem;
        Endereço = endereço;
    }


    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public String getEndereço() {
        return Endereço;
    }

    public void setEndereço(String endereço) {
        Endereço = endereço;
    }

    String Endereço;



    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Contato(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return this.nome ;

    }
}
