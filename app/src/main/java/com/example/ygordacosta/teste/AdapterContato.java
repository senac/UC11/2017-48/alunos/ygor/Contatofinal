package com.example.ygordacosta.teste;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class AdapterContato extends BaseAdapter {
private List<Contato> lista;
private Activity contexto;


    public AdapterContato(Activity contexto, List<Contato>lista) {
        this.contexto = contexto;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int indice) {return this.lista.get(indice);
    }

    @Override
    public long getItemId(int id) {
        int posicao = 0;

        for (int i = 0; i < this.lista.size(); i++) {
            if (this.lista.get(i).getId() == id) {
                posicao = 1;
                break;

            }

        }
            return posicao;
    }

    @Override
    public View getView(int posicao, View convertview, ViewGroup parent) {
         View view = contexto.getLayoutInflater().inflate(R.layout.contato_lista,parent,false);
        TextView txtnome =view.findViewById(R.id.nome);

      Contato contato = this.lista.get(posicao);


         txtnome.setText(contato.getNome());



        return view;
    }
}
